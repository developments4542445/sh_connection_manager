connection_manager

Date: 02/12/2020

Docker container of the connection_manager and its test.

To run locally:

1_ Build the docker:

Use the command:

sudo docker build -t docker_test:1.11 .

2_ Run the docker:

To run, its needed to use docker volumes, privileged mode and interactive bash
Volumes: to add the connection_manager and the main.py test application to the docker.
Privileged modes: to let the docker use the local network to perform the creation of local servers. 

Use the command:

sudo docker run -v <PATH_TO_LOCAL_FOLDER>:<PATH_TO_DOCKER_FOLDER> --privileged -it docker_test:1.11 sh

As an example:

sudo docker run -v /home/fcapdeville/PycharmProjects/Connection_Manager_test/:/usr/share/fede/ --privileged -it docker_test:1.11 sh

2_Run commands:

- dbus-launch

- export $(dbus-launch)

- service dbus start

This has to be manually to ensure that the dbus is running.
If a connection issue is opened due to the local dbus, a message will be displayed.

3_Go to the folder path which was determined in step 1.

4_Confirm ip and port written in the config.json file.

5_Run command:

- ./connection_manager &

If this is not run in docker, should be using sudo.

6_Run command:

- ./main.py

If this is not run in docker, should be using sudo.

It will run the python tests. Logs will be created automatically in the file "Logs:test_app.txt"
